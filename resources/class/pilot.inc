<?php 
/* Contains the logic for all Pilots
 * A class that handles the logic of manipulating Pilots
 * 
 * @author Daniel Waksman
 * @license http://opensource.org/licenses/GPL-3.0  GNU Public Version 3
 * */

class pilot{
	
	//PILOT TABLE
    var $pilot_id;			
	var $pilot_lic_no;		//Licence Number
    var $pilot_name;		//Name
	var $pilot_pic;			//Picture if any
	var $acc_id;			//Associated Account
	var $acc_user;			//
	var $acc_pass;			//

	
    const TABLE = "pilots";

	/*Generates the insert sentence for the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_insert(){
		
		$query = "INSERT INTO ".DB_NAME.".".$this::TABLE."(pilot_lic_no, pilot_name, pilot_pic, acc_id) 
		VALUES ('$this->pilot_lic_no','$this->pilot_name','$this->pilot_pic','$this->acc_id');";
		return $query;
	}
	/* Generates SQL sentence to delete the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_delete(){
		$id = $this->pilot_id;
		$query = "DELETE FROM ".DB_NAME.".".$this::TABLE." WHERE pilot_id = $id;";
		return $query;
	}	

	/* Insert the data instanciated in the structure into de database
	 * @author Daniel Waksman
	 * */
	function insert_yourself(){
		$query = $this->gen_SQL_insert();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
	
	/* Deletes an aircraft as the ID in the structure
	 * @author Daniel Waksman
	 * */
	function delete_yourself(){
		$query = $this->gen_SQL_delete();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
     
}


?>