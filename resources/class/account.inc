<?php 
/* Contains the logic for all Users
 * A class that handles the logic of manipulating Users
 * 
 * @author Daniel Waksman
 * @license http://opensource.org/licenses/GPL-3.0  GNU Public Version 3
 * */

class account{
	
	//ACCOUNT TABLE
    var $acc_id;
	var $acc_user;  		//Username
    var $acc_pass;			//Password
    var $acc_last_login;	//Last Login Timestamp

	
    const TABLE = "accounts";

	/*Generates the insert sentence for the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_insert(){
		$query = "INSERT INTO ".DB_NAME.".".$this::TABLE."(acc_user, acc_pass, acc_last_login) 
		VALUES ('$this->acc_user','$this->acc_pass','0000-00-00 00:00:00');";
		return $query;
	}
    
	/* Generates SQL sentence to delete the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_delete(){
		$id = $this->acc_id;
		$query = "DELETE FROM ".DB_NAME.".".$this::TABLE." WHERE acc_id = $id;";
		return $query;
	}	

	/* Insert the data instanciated in the structure into de database
	 * @author Daniel Waksman
	 * */
	function insert_yourself(){
		$query = $this->gen_SQL_insert();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
	
	/* Deletes an aircraft as the ID in the structure
	 * @author Daniel Waksman
	 * */
	function delete_yourself(){
		$query = $this->gen_SQL_delete();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
     
}


?>