<?php 
/* Contains the logic for all Users
 * A class that handles the logic of manipulating Settings
 * 
 * @author Daniel Waksman
 * @license http://opensource.org/licenses/GPL-3.0  GNU Public Version 3
 * */

class settings{
	
	//SETTING TABLE
	var $pilot_id = 0;  				//PILOT'S SETTINGS
	var $user_lang = 'EN';				//USER LANG - DEFAULT: EN	
	var $user_duty_starts = 90;			//DUTY STARTS X MINUTES BEFORE FLIGHT
	var $user_duty_ends = 30;			//DUTY ENDS X MINUTES AFTER SHUTDOWN
	var $user_def_apt = '';				//DEFAULT AIRPORT
	var $user_def_funct = '';			//DEFAULT FUNCTION	
	var $user_def_flight_rules = '';	//DEFAULT FLIGHT RULES
	var $user_max_on_30 = 0;			//MAX HOURS ON 30 DAYS
	var $user_max_on_year = 0;			//MAX HOURS ON YEAR
	var $user_def_fltno_prefix = '';	//DEFAULT FLIGHT NUMBER PREFIX
	
    const TABLE = "user_config";

	/*Generates the insert sentence for the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_insert(){
		$query = "INSERT INTO ".DB_NAME.".".$this::TABLE."
		(pilot_id, user_def_apt, user_def_funct, user_def_flight_rules, user_max_on_30, user_max_on_year, user_def_fltno_prefix) 
		VALUES ('$this->pilot_id',
		'$this->user_def_apt',
		'$this->user_def_funct',
		'$this->user_def_flight_rules',
		'$this->user_max_on_30',
		'$this->user_max_on_year',
		'$this->user_def_fltno_prefix');";
		return $query;
	}
	
	/*Generates the insert sentence for the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_update(){
		$query = "UPDATE ".DB_NAME.".".$this::TABLE." SET  
		user_lang = '$this->user_lang', 
		user_duty_starts = '$this->user_duty_starts',
		user_duty_ends = '$this->user_duty_ends',
		user_def_apt = '$this->user_def_apt',
		user_def_funct = '$this->user_def_funct',
		user_def_flight_rules = '$this->user_def_flight_rules',
		user_max_on_30 = '$this->user_max_on_30',
		user_max_on_year = '$this->user_max_on_year',
		user_def_fltno_prefix = '$this->user_def_fltno_prefix'
		WHERE pilot_id = '$this->pilot_id';";
		return $query;
	}
    
	/* Generates SQL sentence to delete the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_delete(){
		$id = $this->pilot_id;
		$query = "DELETE FROM ".DB_NAME.".".$this::TABLE." WHERE pilot_id = $id;";
		return $query;
	}	

	/* Insert the data instanciated in the structure into de database
	 * @author Daniel Waksman
	 * */
	function insert_yourself(){
		$query = $this->gen_SQL_insert();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
	
	/* Deletes a setting as the ID in the structure
	 * @author Daniel Waksman
	 * */
	function delete_yourself(){
		$query = $this->gen_SQL_delete();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
	
	/* Updates a setting as the ID in the structure
	 * @author Daniel Waksman
	 * */
	function update_yourself(){
		$query = $this->gen_SQL_update();
		require_once 'methods.inc';
		execSQL_Update($query);
	}
     
}


?>