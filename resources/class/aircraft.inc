<?php 
/* Contains the logic for all Aircraft
 * A class that handles the logic of manipulating Aircrafts
 * 
 * @author Daniel Waksman
 * @license http://opensource.org/licenses/GPL-3.0  GNU Public Version 3
 * */

class aircraft{
	
    var $craft_id;
	var $type_id;  			//Aicraft_type
    var $craft_reg;			//Aircraft Registration eg: CXBBL
    var $craft_markings;	//Aicraft Markings eg: Blanco y Rojo
    var $craft_comments;	//Aircraft Comments
    var $pilot_id;
    
    const TABLE = "aircraft";

	/*Generates the insert sentence for the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_insert(){
		$query = "INSERT INTO ".DB_NAME.".".$this::TABLE."(type_id, craft_reg, craft_markings, craft_comments, pilot_id) 
		VALUES ('$this->type_id','$this->craft_reg', '$this->craft_markings' ,'$this->craft_comments', '$this->pilot_id');";
		return $query;
	}
    
	/* Generates SQL sentence to delete the data instanciated in the structure
	 * @return string $query
	 * @author Daniel Waksman
	 * */
	function gen_SQL_delete(){
		$id = $this->craft_id;
		$query = "DELETE FROM ".DB_NAME.".".$this::TABLE." WHERE craft_id = $id;";
		return $query;
	}	

	/* Insert the data instanciated in the structure into de database
	 * @author Daniel Waksman
	 * */
	function insert_yourself(){
		$query = $this->gen_SQL_insert();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
	
	/* Deletes an aircraft as the ID in the structure
	 * @author Daniel Waksman
	 * */
	function delete_yourself(){
		$query = $this->gen_SQL_delete();
		require_once 'methods.inc';
		execSQL_Insert($query);
	}
     
}


?>