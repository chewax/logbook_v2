<?php
	function add_css_screen(){
		echo'<link rel="stylesheet" type="text/css" href="'.BASE_URL.'/public_html/css/'.CSS_SCREEN.'">';
	}
	function add_css_mobile(){
		echo'<link rel="stylesheet" type="text/css" href="'.BASE_URL.'/public_html/css/'.CSS_MOVIL.'">';
	}
	function add_css_dataTables(){
		//echo'<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">';
		echo'<link rel="stylesheet" type="text/css" href="'.BASE_URL.'/public_html/css/jquery.dataTables.css">';
	}
	function add_css_dateTimePicker(){
		echo'<link rel="stylesheet" type="text/css" href="'.BASE_URL.'/public_html/css/bootstrap-datetimepicker.min.css">';
	}
	function add_raphael_js(){
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/raphael/raphael-min.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/raphael/g.raphael-min.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/raphael/g.bar-min.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/raphael/g.dot-min.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/raphael/g.line-min.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/raphael/g.pie-min.js"></script>';
	}
	function add_custom_js(){
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/custom.js"></script>';
	}
	function add_jquery(){
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/jquery-2.1.0.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/jquery.cookie.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/jquery.dataTables.columnFilter.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/jquery.dataTables.min.js"></script>';
		//echo '<script type="text/javascript"  src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>';
		echo '<script type="text/javascript"  src="'.BASE_URL.'/public_html/js/jquery.maskedinput.js"></script>';
	}
?>


  			