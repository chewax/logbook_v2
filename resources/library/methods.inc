<?php

	/* Tweak to redirect without common issues
	 * @param text $url: The Url to be redirected to
	 * @author Daniel Waksman
	 * */
	function redirect($url) {
    	header("Location: $url");
    	exit;
	}
	
	/* Connects to the database configured in the config.php global variables and performs a $query
	 * @param Text $query the SQL query to be exectued
	 * @returns $arr that can be navigated using 
	 * foreach($arr as $row) {
		 * echo $row['co1'];}
	 * @author Daniel Waksman
	 * */
	function execSQL ($query){
			
		$DBServer = DB_HOST; // e.g 'localhost' or '192.168.1.100'
		$DBUser   = DB_USER;
		$DBPass   = DB_PASS;
		$DBName   = DB_NAME;
		$conn = new mysqli($DBServer, $DBUser, $DBPass, $DBName);
		
		// check connection
		if ($conn->connect_error) {
		  trigger_error('Database connection failed: '  . $conn->connect_error, E_USER_ERROR);
		}
		
		$rs=$conn->query($query);
 
		if($rs === false) {
		  trigger_error('Wrong SQL: ' . $query . ' Error: ' . $conn->error, E_USER_ERROR);
		} else {
		  $arr = $rs->fetch_all(MYSQLI_ASSOC);
		  return $arr;
		}
		
		$conn->close();
	}

	/* Connects to the database configured in the config.php global variables and performs the insert given the query recieved
	 * @param Text $query the SQL query to be exectued
	 * @author Daniel Waksman
	 * */
	function execSQL_Insert ($query){
		
			
		$DBServer = DB_HOST;
		$DBUser   = DB_USER;
		$DBPass   = DB_PASS;
		$DBName   = DB_NAME;
		$conn = new mysqli($DBServer, $DBUser, $DBPass, $DBName);
		
		// check connection
		if ($conn->connect_error) {
		  trigger_error('Database connection failed: '  . $conn->connect_error, E_USER_ERROR);
		}
		
		$rs=$conn->query($query);
 
		if($rs === false) {
		  trigger_error('Wrong SQL: ' . $query . ' Error: ' . $conn->error, E_USER_ERROR);
		}
		
		$id = mysqli_insert_id($conn);
		return $id;
		
		$conn->close();
	}
	
	/* Connects to the database configured in the config.php global variables and performs the insert given the query recieved
	 * @param Text $query the SQL query to be exectued
	 * @author Daniel Waksman
	 * */
	function execSQL_Update ($query){
		
			
		$DBServer = DB_HOST;
		$DBUser   = DB_USER;
		$DBPass   = DB_PASS;
		$DBName   = DB_NAME;
		$conn = new mysqli($DBServer, $DBUser, $DBPass, $DBName);
		
		// check connection
		if ($conn->connect_error) {
		  trigger_error('Database connection failed: '  . $conn->connect_error, E_USER_ERROR);
		}
		
		$rs=$conn->query($query);
 
		if($rs === false) {
		  trigger_error('Wrong SQL: ' . $query . ' Error: ' . $conn->error, E_USER_ERROR);
		}
		
		$conn->close();
	}
	
	/*Calculates Great Circle distance.
	 * @param lat1 = Latitude of point 1 in radians
	 * @param lat2 = Latitude of point 2 in radians
	 * @param lon1 = Longitude of point 1 in radians
	 * @param lon2 = Latitude of point 2 in radians
	 * @author Daniel Waksman
	 * */
	function haversine($lat1,$lat2,$lon1,$lon2){
		//haversine method for calulating great-circle distance.
		//a = sin²(Δφ/2) + cos(φ1)⋅cos(φ2)⋅sin²(Δλ/2)
		//c = 2⋅atan2(√a, √(1−a))
		//d = R⋅c
						
		//where	φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
		//note that angles need to be in radians to pass to trig functions!				
						
		$R = 6371; //KM
		$dLAT = $lat2 - $lat1;
		$dLONG = $lon2 - $lon1;
			
		$a = sin($dLAT/2) * sin($dLAT/2) + cos($lat1) * cos($lat2) * sin($dLONG/2) * sin($dLONG/2);
		$c = 2*atan2(sqrt($a), sqrt(1-$a));
		$dist = $R * $c;
		return $dist;
	}
		
?>

