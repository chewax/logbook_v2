SELECT * FROM (
SELECT l.log_id,t.type_iata, l.log_out, l.log_in, timestampdiff(minute,l.log_out,l.log_in) as flighttime FROM (logbook.log as l, logbook.aircraft as a, logbook.aircraft_type as t)
WHERE l.craft_id = a.craft_id
AND a.type_id = t.type_id
AND l.pilot_id = 1
AND t.type_iata in ('BN2B','PA23','PA27')) as res
WHERE res.flighttime < 0;

SET SQL_SAFE_UPDATES=0;

/*UPDATE logbook.log SET log_in = DATE_ADD(log_in,INTERVAL 1 DAY)
WHERE log.log_id in (
	SELECT log_id FROM (
	SELECT l.log_id,t.type_iata, l.log_out, l.log_in, timestampdiff(minute,l.log_out,l.log_in) as flighttime FROM (logbook.log as l, logbook.aircraft as a, logbook.aircraft_type as t)
	WHERE l.craft_id = a.craft_id
	AND a.type_id = t.type_id
	AND l.pilot_id = 1
	AND t.type_iata in ('BN2B','PA23','PA27')) as res
	WHERE res.flighttime < 0
);*/

SELECT * FROM logbook.log;
/*DELETE FROM logbook.log;*/

SELECT t.type_iata,SUM(timestampdiff(minute,l.log_out,l.log_in)) as grandTot FROM (logbook.log as l, logbook.aircraft as a, logbook.aircraft_type as t) 
WHERE l.pilot_id = 1
AND a.craft_id = l.craft_id
AND a.type_id = t.type_id
GROUP BY t.type_iata;

SELECT sum(timestampdiff(minute,log_out,log_in)) as hours, t.type_iata FROM (logbook.log as l, logbook.aircraft as a, logbook.aircraft_type as t) 
WHERE l.craft_id = a.craft_id 
AND a.type_id = t.type_id
AND l.pilot_id = 1
GROUP BY t.type_iata;

SELECT * FROM (logbook.log as l, logbook.aircraft as a, logbook.aircraft_type as t) 
WHERE l.craft_id = a.craft_id 
AND a.type_id = t.type_id
AND l.pilot_id = 1;

/*ULTIMOS 30 DIAS*/
SELECT sum(timestampdiff(minute,log_out,log_in)) as last90 FROM logbook.log where pilot_id = 1
AND (log_out >= DATE_SUB(now(), INTERVAL 30 DAY));

SELECT log_out from logbook.log where pilot_id = 1
AND (log_out >= DATE_SUB(now(), INTERVAL 30 DAY))
GROUP BY log_out;


/*TOTALES POR DIA EN LOS ULTIMOS 2 ANIOS*/
SELECT dayofmonth(date(log_out)), sum(timestampdiff(minute,log_out,log_in)) as parcial FROM logbook.log where pilot_id = 1
AND (log_out >= DATE_SUB(now(), INTERVAL 730 DAY))
GROUP BY log_out;

/*ALL TIME HOURS*/
SELECT date(log_out), sum(timestampdiff(minute,log_out,log_in)) as minutos FROM logbook.log where pilot_id = 1
GROUP BY log_out;


