<html>
	<?php
		include_once("../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
		include(math.php);
		
		session_start();
		$session_pilot_id = $_SESSION['pilot_id'];
     ?>
	<head>
		<?php add_css_screen();?>
	</head>
	<body>
		<!-- Add Menu Code -->
		<?php do_menu();?>
		
		<div class="placeholder">
			<form class="settings">
			<?php
	
			//LOAD FORM WITH DATA
			$query = "SELECT * FROM ".DB_NAME.".airports";
			$arr = execSQL($query);
	
			foreach ($arr as $row) {
				$apt_id = $row['apt_id'];
				$apt_icao = $row['apt_ICAO'];
				$apt_lat_deg = $row['apt_lat_deg'];
				$apt_lat_min = $row['apt_lat_min'];
				$apt_lat_seg = $row['apt_lat_seg'];
				$apt_lat_dir = $row['apt_lat_dir'];
				$apt_long_deg = $row['apt_long_deg'];
				$apt_long_min = $row['apt_long_min'];
				$apt_long_seg = $row['apt_long_seg'];
				$apt_long_dir = $row['apt_long_dir'];
				
				$apt_lat = $apt_lat_deg + $apt_lat_min/60 + $apt_lat_seg/3600;
				$apt_long = $apt_long_deg + $apt_long_min/60 + $apt_long_seg/3600;
				
				$apt_lat_dir = 'S' ? $apt_lat * -1 : $apt_lat * 1;
				$apt_long_dir = 'E' ? $apt_long * -1 : $apt_long * 1;
				
				$apt_lat_rad = $apt_lat * pi() / 180;
				$apt_long_rad = $apt_long * pi() / 180;
				

				
				echo pi();
				echo $apt_icao;
				echo "<br>";
				
				$qUPD = "UPDATE `".DB_NAME."`.`Airports` SET `apt_lat`='$apt_lat', `apt_long`='$apt_long', `apt_long_rad`='$apt_long_rad', `apt_lat_rad`='$apt_lat_rad' WHERE `apt_Id`='$apt_id';";
				execSQL_Update($qUPD);
				
				echo $qUPD;
				echo "<br>";
				
				echo "LAT: ".$apt_lat;
				echo "<br>";
				echo "LONG: ".$apt_long;
				echo "<br>";
				echo "<br>";				
			}
			?>	
			</form>
		</div>
	</body>
</html>