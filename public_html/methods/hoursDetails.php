<?php
include_once('../../resources/config.php');
include_once("methods.inc");

function getGrandTotal($session_pilot_id){
	//GRAND TOTAL
	$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as grandTot FROM ".DB_NAME.".log where pilot_id = $session_pilot_id;";
	$arr = execSQL($query);
	
	foreach ($arr as $row) {
		$grandTotal_min = $row['grandTot'];
		return round($grandTotal_min/60,2);
	}
}
function getLastXDays($session_pilot_id,$date,$x){
	//LAST X Days
	$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as minutesFlown FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
	AND (log_out >= DATE_SUB('$date', INTERVAL $x DAY)) AND (log_out <= '$date') ;";
	$arr = execSQL($query);
	
	foreach ($arr as $row) {
		$minutesFlown = $row['minutesFlown'];
		return round($minutesFlown/60,2);
	}
}
function getLastXDaysIFR($session_pilot_id,$date,$x){
	//LAST X Days IFR
	$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as minutesFlown FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
	AND (log_out >= DATE_SUB(now(), INTERVAL $x DAY))
	AND log_flight_rules = 'IFR'
	AND log_out <= '$date';";
	$arr = execSQL($query);
	
	foreach ($arr as $row) {
		$minutesFlown = $row['minutesFlown'];
		return round($minutesFlown/60,2);
	}
}
function getLastXDaysVFR($session_pilot_id,$date,$x){
	//LAST YEAR VFR
	$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as minutesFlown FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
	AND (log_out >= DATE_SUB(now(), INTERVAL $x DAY))
	AND log_flight_rules = 'VFR'
	AND log_out <= '$date';";
	$arr = execSQL($query);
	
	foreach ($arr as $row) {
		$minutesFlown = $row['minutesFlown'];
		return round($minutesFlown/60,2);
	}
}
function getHoursByType($session_pilot_id,$date,$type){
	//HOURS BY TYPE
	$query = "SELECT t.type_iata,SUM(timestampdiff(minute,l.log_out,l.log_in)) as total FROM (".DB_NAME.".log as l, ".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t) 
	WHERE l.pilot_id = $session_pilot_id
	AND a.craft_id = l.craft_id
	AND a.type_id = t.type_id
	AND t.type_iata = '$type'";
	$arr = execSQL($query);
		
	foreach ($arr as $row) {
		$tot = $row['total'];
		return round($tot/60,2);
	}

}
function getIFRApproachesLastXDays(){}
function getPICTimeLastXDays(){}
function getSICTimeLastXDays(){}
function getLandingsLastXDays(){}
function getSTULastXDays(){}
function getSIMLastXDays(){}
function getINSLastXDays(){}
function getDeadHeadLastXDays(){}

?>