<?php
require_once '../../resources/config.php';
require(CLASS_PATH."/logbook.inc");
require(CLASS_PATH."/aircraft.inc");
require(CLASS_PATH."/aircraft_type.inc");
include_once("methods.inc");

session_start();
$session_pilot_id = $_SESSION['pilot_id'];
	
$fila = 1;
ini_set('auto_detect_line_endings',TRUE);
if (($gestor = fopen("data/av767.csv", "r")) !== FALSE) {
    while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {
        $numero = count($datos);
        $fila++;

		//Chequeo si el avion existe
		$query = "SELECT * FROM ".DB_NAME.".aircraft WHERE pilot_id = $session_pilot_id
		AND craft_reg = '$datos[3]';";
        $arr = execSQL($query);
        $i = 0;
		$ac = new aircraft();
		$tipo = new aircraft_type();
		
        foreach ($arr as $row) {       	
	    	$ac->craft_id = $row['craft_id'];
			$tipo->type_id = $row['type_id'];
			$i++;          
		}   
		    
		if ($i==0){ //Entonces no existe el avion en la base de datos
		
			// Chequeo si existe el tipo
			$query = "SELECT * FROM ".DB_NAME.".aircraft_type WHERE pilot_id = $session_pilot_id
				AND type_iata = '$datos[2]';";
			$arr = execSQL($query);
			$j = 0;
			
			foreach ($arr as $row) {
			    	$tipo->type_id = $row['type_id'];
			    	$j++;
			}           
			
			if ($j==0){ //Entonces tampoco hay tipo
				//lo agrego
				$tipo->pilot_id = $session_pilot_id;
				$tipo->type_iata = $datos[2];
				$tipo->insert_yourself();
				
				//Recupero el type_id que es autonumber
				$query = "SELECT * FROM ".DB_NAME.".aircraft_type WHERE pilot_id = $session_pilot_id
				AND type_iata = '$datos[2]';";
				$arr = execSQL($query);
				foreach ($arr as $row) {
			    	$tipo->type_id = $row['type_id'];
				}      
							
			}
			
			//ahora agrego al avion

			$ac->type_id = $tipo->type_id;
			$ac->craft_reg = $datos[3];
    		$ac->pilot_id = $session_pilot_id;
	
			$ac->insert_yourself();
			
			//Recuper el craft_id que es autonumber
			$query = "SELECT * FROM ".DB_NAME.".aircraft WHERE pilot_id = $session_pilot_id
			AND craft_reg = '$datos[3]';";
        	$arr = execSQL($query);
        	foreach ($arr as $row) {       	
		    	$ac->craft_id = $row['craft_id'];       
			}
			
	
		}
	       
	    $log = new logbook();
	    
	    $log->craft_id = $ac->craft_id;
	    $log->pilot_id = $session_pilot_id;
	    $log->log_from_ICAO = $datos[4];
	    $log->log_to_ICAO = $datos[5];
	    $log->log_out = $datos[6];
	    $log->log_in = $datos[7];
	    $log->log_function = 'SIC';
	    $log->log_flight_rules = 'IFR';
	    $log->log_TO = $datos[13];
	    $log->log_LAND = $datos[12];
	    $log->log_comm = $datos[15];
	    $log->log_flt_duty = $datos[0];
	
	    $log->insert_yourself();
			
    }
    fclose($gestor);
}
?>

