<?php
    include_once('../../resources/config.php');
	require(CLASS_PATH."/settings.inc");

    session_start();
    $pilot_id = $_SESSION['pilot_id'];
	$user_def_apt = $_POST["user_def_apt"];
	$user_def_funct = $_POST["user_def_funct"];
	$user_def_flight_rules = $_POST["user_def_flight_rules"];
	$user_max_on_30 = $_POST["user_max_on_30"];
	$user_max_on_year = $_POST["user_max_on_year"];
	$user_def_fltno_prefix = $_POST["user_def_fltno_prefix"];
	
	$sett = new settings();
	$sett->pilot_id = $pilot_id;
	$sett->user_def_apt = $user_def_apt;
	$sett->user_def_funct = $user_def_funct;
	$sett->user_def_flight_rules = $user_def_flight_rules;
	$sett->user_max_on_30 = $user_max_on_30;
	$sett->user_max_on_year = $user_max_on_year;
	$sett->user_def_fltno_prefix = $user_def_fltno_prefix;
	
	$sett->update_yourself();
?>