<?php
    //include_once(CLASS_PATH."/aircraft.inc");
    require_once '../../resources/config.php';
    require(CLASS_PATH."/aircraft.inc");
	
    session_start();
    $pilot_id = $_SESSION['pilot_id'];
	$craft_type = $_POST["type"];
	$craft_reg = $_POST["reg"];
	$craft_marks = $_POST["marks"];
	$craft_comm = $_POST["comm"];

    $ac = new aircraft();
	$ac->type_id = $craft_type;
	$ac->craft_reg = $craft_reg;
	$ac->craft_markings = $craft_marks;
	$ac->craft_comments = $craft_comm;
    $ac->pilot_id = $pilot_id;
	
	$ac->insert_yourself();
?>