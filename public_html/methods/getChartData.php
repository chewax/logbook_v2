<?php
	include_once("../../resources/config.php");
	include_once("methods.inc");
    
	function getTotalHoursLast30Days($session_pilot_id,$fecha){
		
		$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as last30 FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
		AND (log_out >= DATE_SUB('$fecha', INTERVAL 30 DAY)) 
		AND (date(log_out) <= '$fecha');";
		
		$arr = execSQL($query);
		
		foreach ($arr as $row){
			return round($row['last30']/60,2);	
		}
	}
	function getTotalHoursLast365Days($session_pilot_id,$fecha){
		
		$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as last365 FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
		AND (log_out >= DATE_SUB('$fecha', INTERVAL 365 DAY)) 
		AND (date(log_out) <= '$fecha');";
		
		$arr = execSQL($query);
		
		foreach ($arr as $row){
			return round($row['last365']/60,2);	
		}
	}
	function getMaxIn30($session_pilot_id){
		$query = "SELECT * FROM ".DB_NAME.".user_config where pilot_id = $session_pilot_id;";
		$arr = execSQL($query);

		foreach ($arr as $row) {
			$hours = $row['user_max_on_30'];
		}
		
		return $hours;
		//return 90;
	}
	function getMaxIn365($session_pilot_id){

		$query = "SELECT * FROM ".DB_NAME.".user_config where pilot_id = $session_pilot_id;";
		$arr = execSQL($query);

		foreach ($arr as $row) {
			$hours = $row['user_max_on_year'];
		}
		
		return $hours;
		//return 1000;
	}
	function getAllTimeTotalJSON($session_pilot_id){
		
		$limit = 90;
		$i = 0;
		$data = array();
		$acumulado = 0;
	

		$query = "SELECT date(log_out) as fecha, sum(timestampdiff(minute,log_out,log_in)) as minutos FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
		GROUP BY log_out;";
		
		$arr = execSQL($query);
		
		foreach ($arr as $row) {
			$fecha = $row['fecha'];
			$horas = round($row['minutos']/60,2);
			$acumulado += $horas;
			$last30 = getTotalHoursLast30Days($session_pilot_id, $fecha);
			$last365 = getTotalHoursLast365Days($session_pilot_id, $fecha);
			$max = getMaxIn30($session_pilot_id);
			$max365 = getMaxIn365($session_pilot_id);
			$data[$i] = array('fecha' => $fecha, 'voladas' => $horas, 'last30' => $last30, 'last365' => $last365, 'limit' => $max, 'limit365'=>$max365, 'grandTotal' => $acumulado);
			$i++;
		}
		
		return json_encode($data);
	}
	function getTotalHoursByAircraftJSON($session_pilot_id){
		
		$i = 0;
		$data = array();
	

		$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as minutos, t.type_iata as craft
		FROM (".DB_NAME.".log as l, ".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t) 
		WHERE l.craft_id = a.craft_id 
		AND a.type_id = t.type_id
		AND l.pilot_id = $session_pilot_id
		GROUP BY t.type_iata;";
		
		$arr = execSQL($query);
		
		foreach ($arr as $row) {
			$horas = round($row['minutos']/60,2);
			$craft = $row['craft'];
			$data[$i] = array('craft' => $craft, 'horas' => $horas);
			$i++;
		}
		
		return json_encode($data);
	}
	
?>