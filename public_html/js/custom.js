/*CONTROL FUNCTIONS*/
function doCheckUser(){
    var acc_user = $('[name="edUser"]').val();
    var acc_pass = $('[name="edPass"]').val();
    
    $.ajax({
        type:"POST",
        url:"/dev/logbook_v2/public_html/methods/checkUser.php",
        data: {acc_user:acc_user,acc_pass:acc_pass},
        success: function (){
        	window.location.href = '/dev/logbook_v2/public_html/main.php';
        }
    });
       
}
function doLogout(){
            
            $.ajax({
                type:"POST",
                url:"/dev/logbook_v2/public_html/methods/doLogout.php",
                data: {},
                success: function (){
                    //location.reload()
                    window.location.href = '/dev/logbook_v2/public_html/main.php';
                }
            });
        }
        
function callRegister(){
	window.location.href = '/dev/logbook_v2/public_html/register.php';
}

function callSettings(){
	window.location.href = '/dev/logbook_v2/public_html/settings.php';
}

/*SHOW/HIDE FORMS*/
function showTypeForm(){
    //escondo los otros
    $('#aircraftForm').hide('fast'); 
    $('#loginForm').hide('fast'); 
    
    if($('#typeForm').css('display') == 'none'){ 
       $('#typeForm').show('fast'); 
    } else { 
       $('#typeForm').hide('fast'); 
    }
}
function showLoginForm(){
    $('#aircraftForm').hide('fast'); 
    $('#typeForm').hide('fast'); 
    $('#logbookForm').hide('fast');
    
    if($('#loginForm').css('display') == 'none'){ 
       $('#loginForm').show('fast'); 
    } else { 
       $('#loginForm').hide('fast'); 
    }
}
function showLogbookForm(){
    $('#aircraftForm').hide('fast'); 
    $('#typeForm').hide('fast'); 
    
    if($('#logbookForm').css('display') == 'none'){ 
       $('#logbookForm').show('fast'); 
    } else { 
       $('#logbookForm').hide('fast'); 
    }
}
function showAircraftForm(){
    //escondo los otros forms
    $('#typeForm').hide('fast');
    $('#loginForm').hide('fast'); 
    
    if($('#aircraftForm').css('display') == 'none'){ 
       $('#aircraftForm').show('fast'); 
    } else { 
       $('#aircraftForm').hide('fast'); 
    }
}

/*DELETE FUNCTIONS*/
function deleteAircraft(craft_id) {
    var didConfirm = confirm("Are you sure you want to delete this Aircraft?");
    if (didConfirm == true) {
        $.ajax({
            type:"POST",
            url:"./dev/logbook_v2/public_html/methods/deleteAircraft.php",
            data: {id:craft_id},
            success: function(){location.reload();}
        });
        
        //Actualizo cookies para mantener el estado de la pagina por si se refresca
        $.cookie('tabType',false); //saco la tabla de Tipos
        $.cookie('tabLogbook',false);   //saco la tabla del libro
        $.cookie('tabCraft',true);   //pongo la tabla de Aviones
        //=================
    }
    
}
function deleteType(type_id) {
    var didConfirm = confirm("Are you sure you want to delete this Type?");
    if (didConfirm == true) {
        $.ajax({
            type:"POST",
            url:"/dev/logbook_v2/public_html/methods/deleteType.php",
            data: {id:type_id},
            success: function(){location.reload();}
        });
        
        //Actualizo cookies para mantener el estado de la pagina por si se refresca
        $.cookie('tabCraft',false); //saco la tabla de Aircraft
        $.cookie('tabLogbook',false);   //saco la tabla del libro
        $.cookie('tabType',true);   //pongo la tabla de Tipos
        //=================
    }
}
function deleteLogEntry(log_id) {
    var didConfirm = confirm("Are you sure you want to delete this log?");
    if (didConfirm == true) {
        $.ajax({
            type:"POST",
            url:"/dev/logbook_v2/public_html/methods/deleteLog.php",
            data: {id:log_id},
            success: function(){location.reload();}
        });
        
        //Actualizo cookies para mantener el estado de la pagina por si se refresca
        $.cookie('tabCraft',false); //saco la tabla de Tipos
        $.cookie('tabType',false);   //saco la tabla de Aviones
        $.cookie('tabLogbook',true);   //pongo la tabla del libro
        //=================
    }
}

/*INSERT FUNCTIONS*/
function insertType() {
    var craft_iata = $('[name="edIata"]').val();
    var craft_name = $('[name="edName"]').val();
    var craft_isMulti = $('[name="isMulti"]').val();
    var craft_isComplex = $('[name="isComplex"]').val();
    var craft_isFixedWing = $('[name="isFixedWing"]').val();
    var craft_isLand = $('[name="isLand"]').val();
    var craft_isLta = $('[name="isLighterThanAir"]').val();
 	$.ajax({
        type:"POST",
        url:"/dev/logbook_v2/public_html/methods/insertType.php",
        data: {iata:craft_iata, name:craft_name, isMulti:craft_isMulti, isComplex:craft_isComplex, isFixedWing:craft_isFixedWing, isLand:craft_isLand, isLta:craft_isLta},
        success: function (){location.reload();}
    });
    //Actualizo cookies para mantener el estado de la pagina por si se refresca
        $.cookie('tabCraft',false); //saco la tabla de Tipos
        $.cookie('tabLogbook',false);   //saco la tabla del libro
        $.cookie('tabType',true);   //pongo la tabla de Aviones
        //=================
}
function insertAircraft() {
    var craft_type = $('[name="crType"]').val();
    var craft_reg = $('[name="edReg"]').val();
    var craft_marks = $('[name="edMarks"]').val();
    var craft_comm = $('[name="edComm"]').val();
 	$.ajax({
        type:"POST",
        url:"/dev/logbook_v2/public_html/methods/insertCraft.php",
        data: {type:craft_type, reg:craft_reg, marks:craft_marks, comm:craft_comm},
        success: function (){location.reload();}
    });
    //Actualizo cookies para mantener el estado de la pagina por si se refresca
    $.cookie('tabType',false); //saco la tabla de Tipos
    $.cookie('tabLogbook',false);   //saco la tabla del libro
    $.cookie('tabCraft',true);   //pongo la tabla de Aviones
    //=================
}
function insertLog() {

    var craft_id  = $('[name="logbookAircraftDDL"]').val();
    //var pilot_id  = $('[name="logbookPilotsDDL"]').val();
    var log_from_ICAO  = $('[name="edFromIATA"]').val();
    var log_to_ICAO = $('[name="edTOIATA"]').val();
    var log_out = $('[name="edLogOUT"]').val();
    var log_off = $('[name="edLogOFF"]').val();
    var log_on = $('[name="edLogON"]').val();
    var log_in = $('[name="edLogIN"]').val();
    var log_function = $('[name="functionOnboardDDL"]').val();
    var log_flight_rules = $('[name="flightRulesDDL"]').val();
    var log_TO = $('[name="edNoTO"]').val();
    var log_LAND = $('[name="edNoLAND"]').val();
    var log_app = $('[name="ApproachTypeDDL"]').val();
    var log_comm = $('[name="logbookCommentTA"]').val();
    var log_flt_no = $('[name="edFltNo"]').val();
    var log_flt_duty = $('[name="flightDutyDDL"]').val();
    
    
    
 	$.ajax({
        type:"POST",
        url:"/dev/logbook_v2/public_html/methods/insertLog.php",
        data: {
            craft_id: craft_id,
            //pilot_id:pilot_id,
            log_from_ICAO:log_from_ICAO,
            log_to_ICAO:log_to_ICAO,
            log_out:log_out,
            log_off:log_off,
            log_on:log_on,
            log_in:log_in,
            log_function:log_function,
            log_flight_rules:log_flight_rules,
            log_TO:log_TO,
            log_LAND:log_LAND,
            log_app:log_app,
            log_comm:log_comm,
            log_flt_no:log_flt_no,
            log_flt_duty:log_flt_duty
        },
        success: function (){location.reload();}
    });
    //Actualizo cookies para mantener el estado de la pagina por si se refresca
        $.cookie('tabCraft',false); //saco la tabla de Tipos
        $.cookie('tabType',false);   //saco la tabla de Aviones
        $.cookie('tabLogbook',true);   //pongo la tabla del libro
        //=================
}

function updateSettings() {

    var user_def_apt  = $('[name="edDefApt"]').val();
    var user_def_funct  = $('[name="edDefFunc"]').val();
    var user_def_flight_rules  = $('[name="edDefFltRules"]').val();
    var user_max_on_30  = $('[name="edMaxOn30"]').val();
    var user_max_on_year  = $('[name="edMaxOnYear"]').val();
    var user_def_fltno_prefix  = $('[name="edFltnoPrefix"]').val();
    
 	$.ajax({
        type:"POST",
        url:"/dev/logbook_v2/public_html/methods/updateSettings.php",
        data: {
            user_def_apt:user_def_apt,
            user_def_funct:user_def_funct,
            user_def_flight_rules:user_def_flight_rules,
            user_max_on_30:user_max_on_30,
            user_max_on_year:user_max_on_year,
            user_def_fltno_prefix:user_def_fltno_prefix,
        },
        success: function (){location.reload();}
    });
    
    //Actualizo cookies para mantener el estado de la pagina por si se refresca
        $.cookie('tabCraft',false); //saco la tabla de Tipos
        $.cookie('tabType',false);   //saco la tabla de Aviones
        $.cookie('tabLogbook',false);   //saco la tabla del libro
        //=================
}

function newUser() {
    var acc_user = $('[name="edUser"]').val();
    var acc_pass = $('[name="edPass"]').val();
    var acc_email = $('[name="edEmail"]').val();
    var pilot_name = $('[name="edName"]').val();
    var pilot_surname = $('[name="edSurname"]').val();
    var pilot_lic = $('[name="edLicNo"]').val();
    
 	$.ajax({
        type:"POST",
        url:"/dev/logbook_v2/public_html/methods/insertUser.php",
        data: {username:acc_user, password:acc_pass, email:acc_email, pilot_name:pilot_name, pilot_surname:pilot_surname, pilot_lic:pilot_lic},
        success: function (){location.reload();}
    });
    //Actualizo cookies para mantener el estado de la pagina por si se refresca
    $.cookie('tabType',false); //saco la tabla de Tipos
    $.cookie('tabLogbook',false);   //saco la tabla del libro
    $.cookie('tabCraft',true);   //pongo la tabla de Aviones
    //=================
}

/*REPORT FUNCTIONS*/
function switchReports(){
	var selOpt = document.getElementById('ReportOption').value;
	if (selOpt==1){ //grand total
    	$.ajax({
                type:"POST",
                url:"/dev/logbook_v2/public_html/modules/reports/grandTotal.php",
                data: {},
                success: function (){
                    location.reload();
                }
            });
	}
	if (selOpt==2){ //grand total
    	$.ajax({
                type:"POST",
                url:"/dev/logbook_v2/public_html/modules/reports/genCsv.php",
                data: {},
                success: function (){
                    location.reload();
                }
            });
	}
}
