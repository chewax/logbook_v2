<html>
	<?php
		include_once("../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
		
		session_start();
		$session_pilot_id = $_SESSION['pilot_id'];
     ?>
	<head>
		<?php add_css_screen();?>
	</head>
	<body>
		<!-- Add Menu Code -->
		<?php do_menu();?>
		
		<div class="placeholder">
			<form class="settings">
			<?php
	
			//LOAD FORM WITH DATA
			$query = "SELECT * FROM ".DB_NAME.".user_config where pilot_id = $session_pilot_id;";
			$arr = execSQL($query);
	
			foreach ($arr as $row) {
				$user_lang = $row['user_lang'];
				$user_duty_starts = $row['user_duty_starts'];
				$user_duty_ends = $row['user_duty_ends'];
				$user_def_apt = $row['user_def_apt'];
				$user_def_funct = $row['user_def_funct'];
				$user_def_flight_rules = $row['user_def_flight_rules'];
				$user_max_on_30 = $row['user_max_on_30'];
				$user_max_on_year = $row['user_max_on_year'];
				$user_def_fltno_prefix = $row['user_def_fltno_prefix'];
				
				echo '<table>';
					/*
	                echo '<tr>';
						echo '<td>';
						echo '<label>Language:</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_lang.'" id="textbox" placeholder="username" name="edLang">';
						echo '</td>';
					echo '</tr>';
	                
					echo '<tr>';
						echo '<td>';
						echo '<label>Duty starts (minutes before flight)</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_duty_starts.'" id="textbox" placeholder="username" name="edDutyStart">';
						echo '</td>';
					echo '</tr>';
					
					echo '<tr>';
						echo '<td>';
						echo '<label>Duty ends (minutes after shutdown)</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_duty_ends.'" id="textbox" placeholder="username" name="edDutyEnd">';
						echo '</td>';
					echo '</tr>';
					*/
					echo '<tr>';
						echo '<td>';
						echo '<label>Default airport ICAO:</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_def_apt.'" id="textbox" placeholder="ICAO" name="edDefApt">';
						echo '</td>';
					echo '</tr>';
					
					echo '<tr>';
						echo '<td>';
						echo '<label>Default onboard function:</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_def_funct.'" id="textbox" placeholder="PIC,SIC,STU,INS" name="edDefFunc">';
						echo '</td>';
					echo '</tr>';
					
					echo '<tr>';
						echo '<td>';
						echo '<label>Default flight rules:</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_def_flight_rules.'" id="textbox" placeholder="IFR,VFR" name="edDefFltRules">';
						echo '</td>';
					echo '</tr>';
					
					echo '<tr>';
						echo '<td>';
						echo '<label>Max hours on 30 days to Date:</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_max_on_30.'" id="textbox" placeholder="" name="edMaxOn30">';
						echo '</td>';
					echo '</tr>';
					
					echo '<tr>';
						echo '<td>';
						echo '<label>Max hours on Year to Date:</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_max_on_year.'" id="textbox" placeholder="" name="edMaxOnYear">';
						echo '</td>';
					echo '</tr>';
					
					echo '<tr>';
						echo '<td>';
						echo '<label>Default flight number prefix:</label>';
						echo '</td>';
						echo '<td>';
						echo '<input class="edit" type="text" value="'.$user_def_fltno_prefix.'" id="textbox" placeholder="" name="edFltnoPrefix">';
						echo '</td>';
					echo '</tr>';
					
					echo '<tr>';
						echo '<td></td>';
						echo '<td>';
							echo '<input class="btn" type="button" value="Update" name="btnUpdate" onclick="updateSettings()">';
						echo '</td>';
					echo '</tr>';
				echo '<table>';
			}
			?>	
			</form>
		</div>
	</body>
</html>