<?php 
	include_once("../resources/config.php");
	include_once("methods.inc");
	include_once("snippets.inc");
?>
<?php 
	function do_menu(){ 
	add_custom_js();
	add_jquery();
?>

<script type="text/javascript">
    $(document).keyup(function(e) {
      if (e.keyCode == 27) { //Si es escape, cierro todos los forms
        $('#aircraftForm').hide('fast');
        $('#typeForm').hide('fast');
        $('#loginForm').hide('fast');
        $('#logbookForm').hide('fast'); 
      }
    });
</script>

<div class="menu">
	 	<?php
            session_start();
            if (!empty($_SESSION['pilot_id'])){
                $session_pilot_id = $_SESSION['pilot_id'];
                $session_pilot_name = $_SESSION['pilot_name'];
                echo "<div class='menu' id='user_logged'>Hi $session_pilot_name !</div>";
            } else { 
                $session_pilot_id = 0;
                $session_pilot_name = "";
            }
        ?>
        
        <!--<input type="button" class="menu" id="button_login" value="Login" onClick="showLoginForm()">-->
        
        <?php
            if ($session_pilot_id) { //si hay alguien logueado muestro los tabs, si no no
                echo '<a class="tab" href='.BASE_URL.'/public_html/main.php>Home</a>';
				echo '<a class="tab" href='.BASE_URL.'/public_html/modules/types.php>Types</a>';
				echo '<a class="tab" href='.BASE_URL.'/public_html/modules/aircrafts.php>Aircrafts</a>';
				echo '<a class="tab" href='.BASE_URL.'/public_html/modules/logbook.php>Logbook</a>';
				echo '<a class="tab" href='.BASE_URL.'/public_html/modules/reports.php>Reports</a>';
				echo '<a class="tab" href='.BASE_URL.'/public_html/modules/funFacts.php>Fun Facts</a>';
				//echo '<a class="tab" href='.BASE_URL.'/public_html/modules/import.php>Imports</a>';
				echo '<input type="button" class="menu" id="button_login" value="Logout" onClick="doLogout()">';
				echo '<input type="button" class="menu" id="button_login" value="Settings" onClick="callSettings()">';
			} else {
				echo '<input type="button" class="menu" id="button_login" value="Login" onClick="showLoginForm()">';
			}
        ?>
  		
  		
  		
  			
        <div class="callout_form loginForm" id="loginForm">
            <form>
                <?php
                    if ($session_pilot_id) { //si hay alguien logueado le ofrezco desloguear si no, el login
                        echo '<input type="button" id="button" value="Logout" name="btnLogin" onclick="doLogout()">';}
                    else {
                        echo '<input type="text" id="textbox" placeholder="username" name="edUser">';
                        echo '<input type="password" id="textbox" placeholder="password" name="edPass">';
                        echo '<input type="button" id="button" value="Login" name="btnLogin" onclick="doCheckUser()">';
						echo '<div class="login_form_footer">';
                        echo '<a href="javascript:void(0);" onclick="callRegister()">Register</a> ::';
						echo '<a href="javascript:void(0);" onclick="callRegister()">Forgot Password!</a>';
						echo '</div>';
                         }  
                ?>
                
            </form>
        </div>
</div>
<?php } ?>