<?php
require_once '../../../resources/config.php';
include_once("methods.inc");
include_once("pdf/fpdf.php");

	
session_start();
$session_pilot_id = $_SESSION['pilot_id'];
$y = 0;
$line_height = 7;

//PDF CLASS CONFIG

//Armo un nombre random
$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
$string = '';
for ($i = 0; $i < 15; $i++) {
	$string .= $characters[rand(0, strlen($characters) - 1)];
}	

$string = 'output/'.$string.'.pdf';
$pdf = new FPDF();
$pdf->AddPage();
//================


//GRAND TOTAL
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as grandTot FROM ".DB_NAME.".log where pilot_id = $session_pilot_id;";
$arr = execSQL($query);

foreach ($arr as $row) {
	$grandTotal_min = $row['grandTot'];
}

$hours = floor($grandTotal_min / 60);
$min = $grandTotal_min - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Grand Total: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+20;
$pdf->SetY($y);
//==========================


//LAST 3 MONTHS
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as last90days FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 90 DAY));";
$arr = execSQL($query);

foreach ($arr as $row) {
	$last90days = $row['last90days'];
}

$hours = floor($last90days / 60);
$min = $last90days - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 90 days: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$pdf->SetY($y);
//==========================

//LAST 3 MONTHS IFR
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as last90daysIFR FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 90 DAY))
AND log_flight_rules = 'IFR';";
$arr = execSQL($query);

foreach ($arr as $row) {
	$last90daysIFR = $row['last90daysIFR'];
}
$hours = floor($last90daysIFR / 60);
$min = $last90daysIFR - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 90 days IFR: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$pdf->SetY($y);
//==========================


//LAST 3 MONTHS VFR
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as last90daysVFR FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 90 DAY))
AND log_flight_rules = 'VFR';";
$arr = execSQL($query);

foreach ($arr as $row) {
	$last90daysVFR = $row['last90daysVFR'];
}
$hours = floor($last90daysVFR / 60);
$min = $last90daysVFR - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 90 days VFR: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$y = $y+$line_height;
$pdf->SetY($y);
//==========================



//LAST YEAR
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as lastyear FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 365 DAY));";
$arr = execSQL($query);

foreach ($arr as $row) {
	$lastyear = $row['lastyear'];
}

$hours = floor($lastyear / 60);
$min = $lastyear - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 12 Months: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$pdf->SetY($y);
//==========================


//LAST YEAR IFR
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as lastyearIFR FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 365 DAY))
AND log_flight_rules = 'IFR';";
$arr = execSQL($query);

foreach ($arr as $row) {
	$lastyearIFR = $row['lastyearIFR'];
}

$hours = floor($lastyearIFR / 60);
$min = $lastyearIFR - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 12 Months IFR: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$pdf->SetY($y);
//==========================

//LAST YEAR VFR
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as lastyearVFR FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 365 DAY))
AND log_flight_rules = 'VFR';";
$arr = execSQL($query);

foreach ($arr as $row) {
	$lastyearVFR = $row['lastyearVFR'];
}

$hours = floor($lastyearVFR / 60);
$min = $lastyearVFR - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 12 Months VFR: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$y = $y+$line_height;
$pdf->SetY($y);
//==========================

//===================================================
//LAST 2 YEARS
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as lastyear FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 730 DAY));";
$arr = execSQL($query);

foreach ($arr as $row) {
	$lastyear = $row['lastyear'];
}

$hours = floor($lastyear / 60);
$min = $lastyear - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 24 Months: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$pdf->SetY($y);
//==========================


//LAST 2 YEARS IFR
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as lastyearIFR FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 730 DAY))
AND log_flight_rules = 'IFR';";
$arr = execSQL($query);

foreach ($arr as $row) {
	$lastyearIFR = $row['lastyearIFR'];
}

$hours = floor($lastyearIFR / 60);
$min = $lastyearIFR - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 24 Months IFR: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$pdf->SetY($y);
//==========================

//LAST 2 YEARS VFR
$query = "SELECT sum(timestampdiff(minute,log_out,log_in)) as lastyearVFR FROM ".DB_NAME.".log where pilot_id = $session_pilot_id
AND (log_out >= DATE_SUB(now(), INTERVAL 730 DAY))
AND log_flight_rules = 'VFR';";
$arr = execSQL($query);

foreach ($arr as $row) {
	$lastyearVFR = $row['lastyearVFR'];
}

$hours = floor($lastyearVFR / 60);
$min = $lastyearVFR - ($hours*60);
$output = $hours.':'.$min;

//Titulo
$pdf->SetFont('Courier','B',16);
$pdf->Write(20, 'Last 24 Months VFR: ');
//Horas
$pdf->SetFont('Courier','',16);
$pdf->Write(20, $output);
$y = $y+$line_height;
$y = $y+$line_height;
$pdf->SetY($y);
//==========================


//HOURS BY TYPE
$query = "SELECT t.type_iata,SUM(timestampdiff(minute,l.log_out,l.log_in)) as total FROM (".DB_NAME.".log as l, ".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t) 
WHERE l.pilot_id = $session_pilot_id
AND a.craft_id = l.craft_id
AND a.type_id = t.type_id
GROUP BY t.type_iata;";
$arr = execSQL($query);

	//Titulo
	$pdf->SetFont('Courier','B',16);
	$pdf->Write(20, 'Totals by type:');
	$y = $y+$line_height;
	$pdf->SetY($y);
	
foreach ($arr as $row) {
	$iata = $row['type_iata'];
	$tot = $row['total'];
	
	$hours = floor($tot / 60);
	$min = $tot - ($hours*60);
	$output = $hours.':'.$min;

	//Titulo
	$pdf->SetFont('Courier','B',16);
	$pdf->Write(20, $iata.': ');
	//Horas
	$pdf->SetFont('Courier','',16);
	$pdf->Write(20, $output);
	
	$y = $y+$line_height;
	$pdf->SetY($y);
}

//SE IMPRIME
$pdf->Output($string,'F');

echo $string;
$_SESSION['report_name'] = $string;

?>