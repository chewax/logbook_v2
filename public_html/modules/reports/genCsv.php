<?php
require_once '../../../resources/config.php';
include_once("methods.inc");

	
session_start();
$session_pilot_id = $_SESSION['pilot_id'];


//Armo un nombre random
$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
$string = '';
for ($i = 0; $i < 15; $i++) {
	$string .= $characters[rand(0, strlen($characters) - 1)];
}	

$string = 'output/'.$string.'.csv';
$fp = fopen($string, 'w');
//================

$headers = array("FLIGHT No","FROM","TO","OUT","OFF","ON","IN","FUNCTION","FLIGHT RULES","T/Os","LANDINGs","APPs","COMMENTS","FLIGHT DUTY");

//HEADERS
fputcsv($fp, $headers, ';');

//GRAND TOTAL
$query = "SELECT log_flt_no,log_from_ICAO,log_to_ICAO,log_out,log_off,log_on,log_in,log_function,log_flight_rules,log_TO,log_LAND,log_app,log_comm,log_flt_duty FROM ".DB_NAME.".log;";
$arr = execSQL($query);

foreach ($arr as $row) {
	fputcsv($fp, $row, ';');
}

fclose($fp);
$_SESSION['report_name'] = $string;

?>