<html>
	<?php
		include_once("../../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once("menu.php");
		include_once(PUBLIC_PATH."/methods/getChartData.php");
		include_once(PUBLIC_PATH."/methods/hoursDetails.php");
		
        //USER CONTROL
        session_start();
        if (!empty($_SESSION['pilot_id'])){
            $session_pilot_id = $_SESSION['pilot_id'];
            $session_pilot_name = $_SESSION['pilot_name'];
        } else { 
            $session_pilot_id = 0;
            $session_pilot_name = "";
        }
     ?>
        
	<head>
		<?php add_css_screen();?>
		
		<script src="<?php echo BASE_URL?>/resources/library/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="<?php echo BASE_URL?>/resources/library/amcharts/serial.js" type="text/javascript"></script>
        <script src="<?php echo BASE_URL?>/resources/library/amcharts/pie.js" type="text/javascript"></script>
     
		<!-- GRAFICAS -->
		<script type="text/javascript">

            var chart;
            var chartData = [];
            var acftChartData = [];

            AmCharts.ready(function () {
            	
				// load the data
			    chartData = <?php echo getAllTimeTotalJSON($session_pilot_id); ?>;
			    acftChartData = <?php echo getTotalHoursByAircraftJSON($session_pilot_id); ?>; 		
					    
			    
			    //PIE CHART - AIRCRAFT FLOWN
			    var acftChart = new AmCharts.AmPieChart();
                acftChart.dataProvider = acftChartData;
                acftChart.titleField = "craft";
                acftChart.valueField = "horas";
                acftChart.depth3D = 10;
                acftChart.angle = 45
                
                // AICRAFT CHART - LEGEND
                var acftlegend = new AmCharts.AmLegend();
                acftlegend.align = "center";
                acftlegend.markerType = "circle";
                acftChart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                acftChart.addLegend(acftlegend);
                
		    	// SERIAL CHART
	            chart = new AmCharts.AmSerialChart();
	            chart.pathToImages = "<?php echo BASE_URL?>/resources/library/amcharts/images/";
	            chart.dataProvider = chartData;
	            chart.categoryField = "fecha";
	            chart.dataDateFormat = "YYYY-MM-DD";
	
	            // data updated event will be fired when chart is first displayed,
	            // also when data will be updated. We'll use it to set some
	            // initial zoom
	            chart.addListener("dataUpdated", zoomChart);
	
	            // AXES
	            // Category
	            var categoryAxis = chart.categoryAxis;
	            categoryAxis.parseDates = true; // in order char to understand dates, we should set parseDates to true
	            categoryAxis.minPeriod = "mm"; // as we have data with minute interval, we have to set "mm" here.			 
	            categoryAxis.gridAlpha = 0.07;
	            categoryAxis.axisColor = "#DADADA";
	
	            // Value
	            var valueAxis = new AmCharts.ValueAxis();
	            valueAxis.gridAlpha = 0.07;
	            valueAxis.title = "Flown Hours";
	            chart.addValueAxis(valueAxis);
	
	            // GRAPH 1 = FLOWN HOURS
	            var graph = new AmCharts.AmGraph();
	            graph.type = "line"; // try to change it to "column"
	            graph.title = "Flown Hours";
	            graph.valueField = "voladas";
	            graph.lineAlpha = 1;
	            graph.lineColor = "#0066FF";
	            graph.fillAlphas = 1; // setting fillAlphas to > 0 value makes it area graph
	            chart.addGraph(graph);
	            
	            // GRAPH 2 = LAST 30
	            var graph = new AmCharts.AmGraph();
	            graph.type = "line"; // try to change it to "column"
	            graph.title = "Last 30 days";
	            graph.valueField = "last30";
	            graph.lineAlpha = 1;
	            graph.lineColor = "#009900";
	            graph.fillAlphas = 0.2; // setting fillAlphas to > 0 value makes it area graph
	            chart.addGraph(graph);
	            
	            // GRAPH 3 = LAST 365
	            var graph = new AmCharts.AmGraph();
	            graph.type = "line"; // try to change it to "column"
	            graph.title = "Last 365 days";
	            graph.valueField = "last365";
	            graph.lineAlpha = 1;
	            graph.lineColor = "#FFCC00";
	            graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
	            chart.addGraph(graph);
	            
	            // GRAPH 4 = LIMITS
	            var graph = new AmCharts.AmGraph();
	            graph.type = "line"; // try to change it to "column"
	            graph.title = "Limit in 30";
	            graph.valueField = "limit";
	            graph.lineAlpha = 1;
	            graph.lineColor = "#FF0000";
	            graph.fillAlphas = 0; // setting fillAlphas to > 0 value makes it area graph
	            chart.addGraph(graph);	
	            
	            // GRAPH 5 = LIMIT 365
	            var graph = new AmCharts.AmGraph();
	            graph.type = "line"; // try to change it to "column"
	            graph.title = "Limit in 365";
	            graph.valueField = "limit365";
	            graph.lineAlpha = 1;
	            graph.lineColor = "#FF9900";
	            graph.fillAlphas = 0; // setting fillAlphas to > 0 value makes it area graph
	            chart.addGraph(graph);	
	            
	            // GRAPH 6 = GRAND TOTAL
	            var graph = new AmCharts.AmGraph();
	            graph.type = "smoothedLine"; // try to change it to "column"
	            graph.lineThickness = 2;
	            graph.title = "Grand Total";
	            graph.valueField = "grandTotal";
	            graph.lineAlpha = 1;
	            graph.lineColor = "#33CCFF";
	            graph.fillAlphas = 0; // setting fillAlphas to > 0 value makes it area graph
	            chart.addGraph(graph);	
		
	            // CURSOR
	            var chartCursor = new AmCharts.ChartCursor();
	            chartCursor.cursorPosition = "mouse";
	            chartCursor.categoryBalloonDateFormat = "DD MMMM";
	            chart.addChartCursor(chartCursor);
	
	            // SCROLLBAR
	            var chartScrollbar = new AmCharts.ChartScrollbar();
	            chart.addChartScrollbar(chartScrollbar);
	
	            // LEGEND
			    var legend = new AmCharts.AmLegend();
			    legend.align = "center";
                legend.markerType = "circle";
			    chart.addLegend(legend, "legend30");
			    
	            // WRITE
	            chart.write("flightHistory");
                acftChart.write("aircraftFlown");
	            
	        });

	        // this method is called when chart is first inited as we listen for "dataUpdated" event
	        function zoomChart() {
	            // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
	            chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
	        }
		</script>
	</head>
	
	<body>
		 <?php do_menu();?>
        <div id="flightHistory" style="margin-top:20px; width: 100%; height: 300px;"></div>
        <div id="legend30" style="width: 90%;"></div>
		<div id="secondLine">
			<div id="aircraftFlown" style="margin-top:20px; width: 50%; height: 300px; float:left"></div>
			<div class="resumen" style="margin-top:20px; height: 300px; float:left">
				<table>
					<thead>
						<tr>
							<td></td><td>Hours</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="columnHead">Last 30 Days:</td>
							<td><?php 
								$hrtotal =  getLastXDays($session_pilot_id, date("Y-m-d H:i:s"), 30);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
						<tr>
							<td class="columnHead">Last 30 Days IFR:</td>
							<td><?php 
								$hrtotal =  getLastXDaysIFR($session_pilot_id, date("Y-m-d H:i:s"), 30);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
						<tr>
							<td class="columnHead">Last 30 Days VFR:</td>
							<td><?php 
								$hrtotal =  getLastXDaysVFR($session_pilot_id, date("Y-m-d H:i:s"), 30);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
					</tbody>
				</table>
				
				<table>
					<thead>
						<tr>
							<td></td><td>Hours</td>
						</tr>
					</thead>
					<tbody>
						<tr>
						<tr>
							<td class="columnHead">Last 60 Days:</td>
							<td><?php 
								$hrtotal =  getLastXDays($session_pilot_id, date("Y-m-d H:i:s"), 60);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
						<tr>
							<td class="columnHead">Last 60 Days IFR:</td>
							<td><?php 
								$hrtotal =  getLastXDaysIFR($session_pilot_id, date("Y-m-d H:i:s"), 60);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
						<tr>
							<td class="columnHead">Last 60 Days VFR:</td>
							<td><?php 
								$hrtotal =  getLastXDaysVFR($session_pilot_id, date("Y-m-d H:i:s"), 60);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
					</tbody>
				</table>
				
				<table>
					<thead>
						<tr>
							<td></td><td>Hours</td>
						</tr>
					</thead>
					<tbody>	
						<tr>
							<td class="columnHead">Last Year:</td>
							<td><?php 
								$hrtotal =  getLastXDays($session_pilot_id, date("Y-m-d H:i:s"), 365);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
						<tr>
							<td class="columnHead">Last Year IFR:</td>
							<td><?php 
								$hrtotal =  getLastXDaysIFR($session_pilot_id, date("Y-m-d H:i:s"), 365);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
						<tr>
							<td class="columnHead">Last Year VFR:</td>
							<td><?php 
								$hrtotal =  getLastXDaysVFR($session_pilot_id, date("Y-m-d H:i:s"), 365);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
                    </tbody>
				</table>
                <table>
					<thead>
						<tr>
							<td></td><td>Hours</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="columnHead">Grand Total:</td>
							<td><?php 
								$hrtotal =  getGrandTotal($session_pilot_id);
								$hours = floor($hrtotal);
								$min = round(($hrtotal - $hours)*60);
								echo $hours.':'.$min;
								?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="personal_info" style="margin-top:20px; width: 30%; height: 300px; float:left">
			</div>
		</div>
	</body>
</html>