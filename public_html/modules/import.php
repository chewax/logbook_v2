<html>
	<?php
		include_once("../../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
	?>
	<head>
		<!-- CSS -->
		<?php add_css_screen();?>
		<?php add_css_dataTables();?>
		
	</head>
	<body>
		<?php do_menu();?>
		<script>
			$(function(){
				$("#typesTable").dataTable();
				$('#typesTable').dataTable().columnFilter();
			})
		</script>
		<?php
            session_start();
            if (!empty($_SESSION['pilot_id'])){
                $session_pilot_id = $_SESSION['pilot_id'];
                $session_pilot_name = $_SESSION['pilot_name'];
            } else { 
                $session_pilot_id = 0;
                $session_pilot_name = "";
            }
        ?>
	</body>
</html>