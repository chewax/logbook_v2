<html>
	<?php
		include_once("../../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
	?>
	<head>
		<!-- CSS -->
		<?php add_css_screen();?>
		<?php add_css_dataTables();?>
	</head>
	<body>
		<?php do_menu();?>
		<script>
			$(function(){
				$("#aircraftsTable").dataTable();
				$('#aircraftsTable').dataTable().columnFilter();
			})
		</script>

		<?php
			//Chequeo sesion para no mostrar si no hay nadie logueado.
            session_start();
            if (!empty($_SESSION['pilot_id'])){
                $session_pilot_id = $_SESSION['pilot_id'];
                $session_pilot_name = $_SESSION['pilot_name'];
            } else { 
                $session_pilot_id = 0;
                $session_pilot_name = "";
            }
        ?>
        
		<div class="display" id="tabcraft">
			
           	<!--Formulario para ingresar aviones-->
			<div class="callout_form" id="aircraftForm">
                <form method="post" action="#">
                    Type:
                    <select name="crType" id="dropdownlist">
                    <?php
                        $query = "SELECT * FROM ".DB_NAME.".aircraft_type WHERE pilot_id = $session_pilot_id;";
                        $arr = execSQL($query);
                        $i = 1;
                        foreach ($arr as $row) {
                            ($i % 2)== 0 ? $class = "grid_evenrow" : $class = "grid_oddrow";
                            $type_id = $row['type_id'];
                            $type_iata = $row['type_iata'];
                            $type_name = $row['type_name'];
                            echo "<option value='$type_id'>$type_iata - $type_name</option>";
                        }
                    ?>
                    </select>
                    <br>
                    <input id='textbox' type="text" placeholder="Registration, eg: N123LA" name="edReg"/>
                    <br>
                    <input id='textbox' type="text" placeholder="Markings, eg: White and Red" name="edMarks"/>
                    <br>
                    <textarea cols="30" rows="10" id="freetext" name="edComm" placeholder="comments"></textarea>
                    <br>
                    <input type="button" name="submit" id="button" value="Insertar" onclick="insertAircraft()">
                </form>	
            </div>
            <!--Formulario para Filtrar-->
            
            
            
            <!--Tabla para desplegar resultados-->
			<table class="grid" id="aircraftsTable">
			<thead>
            <tr>
                <!--<th class='grid'>Aircraft id</th>
                <th class='grid'>Aircraft Type id</th>-->
                <th class='grid'>Aircraft Registration</th>
                <th class='grid'>Type IATA</th>
                <th class='grid'>Type Name</th>
                <th class='grid'>Aircraft Markings</th>
                <th class='grid'>Comments</th>
                
                <?php 
                    if ($session_pilot_id) {
                        echo '<th class="grid"><a href="javascript:void(0);" onclick="showAircraftForm()" class="add">+</a></th>';
                    }else{
                        echo '<th class="grid"></th>';
                    }
                ?>

                
            </tr>
            </thead>
            <tbody>

                <?php
                $query = "SELECT * FROM (".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t)
                WHERE a.type_id = t.type_id
                AND a.pilot_id = $session_pilot_id;";

                $arr = execSQL($query);
                $i = 1;
                foreach ($arr as $row) {
                    ($i % 2)== 0 ? $class = "grid_evenrow" : $class = "grid_oddrow";
                    $craft_id = $row['craft_id'];
                    $type_id = $row['type_id'];
                    $craft_reg = $row['craft_reg'];
                    $craft_markings = $row['craft_markings'];
					$craft_comments = $row['craft_comments'];
                    $type_iata = $row['type_iata'];
                    $type_name = $row['type_name'];
                    //echo "<td class='$class'>$craft_id</td>";
                    //echo "<td class='$class'>$type_id</td>";
                    echo "<td class='$class'>$craft_reg</td>";
                    echo "<td class='$class'>$type_iata</td>";
                    echo "<td class='$class'>$type_name</td>";
                    echo "<td class='$class'>$craft_markings</td>";
                    echo "<td class='$class'>$craft_comments</td>";
                    echo "<td class='$class'><a class='rowdel' onclick='deleteAircraft($craft_id)' href='javascript:void(0);'>delete</a></td>";
                    echo"</tr>";
                    $i++;
                }
            ?>
            </tbody>
            
            <tfoot>
            	<tr>
                <th id='footer'>Aircraft id</th>
                <th id='footer'>Aircraft Type id</th>
                <th id='footer'>Aircraft Registration</th>
                <th id='footer'>Type IATA</th>
                <th id='footer'>Type Name</th>
                <th id='footer'>Aircraft Markings</th>
                
            </tr>
            	
            </tfoot>
            </table>
 
 
</div>
	</body>
</html>