<html>
	<?php
		include_once("../../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
		
		session_start();
            if (!empty($_SESSION['pilot_id'])){
                $session_pilot_id = $_SESSION['pilot_id'];
                $session_pilot_name = $_SESSION['pilot_name'];
            } else { 
                $session_pilot_id = 0;
                $session_pilot_name = "";
        }
	?>
	<head>
		<?php add_css_screen();?>
		<?php add_css_dataTables();?>
	</head>
	<body>
		<?php do_menu();?>

		<div class="display" id="funFacts">   
			
			<form method="post" action="#">
				<label>The airport in which you have taken off the most is</label>
				<?php
                    $query = "SELECT SUM(l.log_TO) AS TONo, l.log_from_ICAO AS ICAO, a.apt_Name as Nom
                    FROM ".DB_NAME.".log as l, ".DB_NAME.".Airports as a 
					WHERE  l.pilot_id = $session_pilot_id
					AND l.log_from_ICAO = a.apt_ICAO
					GROUP BY l.log_from_ICAO
					ORDER BY TONo DESC 
					LIMIT 1;";

                    $arr = execSQL($query);
					
                    foreach ($arr as $row) {
                        $TOs = $row['TONo'];
                        $APT = $row['ICAO'];
						$Name = $row['Nom'];
                        echo '<label>'.$APT.': '.$Name.' with '.$TOs.' takeoffs!</label>';
                    }
                ?>
               </br>
                <label>The airport in which you have landed the most is</label>
				<?php
                    $query = "SELECT SUM(log.log_LAND) AS LDGS, log.log_to_ICAO AS ICAO , a.apt_Name as Nom 
                    FROM ".DB_NAME.".log, ".DB_NAME.".Airports as a
					WHERE  log.pilot_id = $session_pilot_id
					AND log.log_to_ICAO = a.apt_ICAO
					GROUP BY log.log_to_ICAO
					ORDER BY LDGS DESC 
					LIMIT 1;";

                    $arr = execSQL($query);
					
                    foreach ($arr as $row) {
                        $Ldgs = $row['LDGS'];
                        $APT = $row['ICAO'];
                        $Name = $row['Nom'];
                        echo '<label>'.$APT.': '.$Name.' with '.$Ldgs.' landings!</label>';
                    }
                ?>
			</br>
                <label>You have performed</label>
				<?php
                    $query = "SELECT sum(log.log_LAND) AS LDGS from ".DB_NAME.".log
					WHERE log.pilot_id = $session_pilot_id;";
					
                    $arr = execSQL($query);
					
                    foreach ($arr as $row) {
                        $Ldgs = $row['LDGS'];
                        echo '<label> '.$Ldgs.' landings in all your career!</label>';
                    }
                ?>
            </br>
                <label>And flown</label>
				<?php
                    $query = "SELECT l.log_from_ICAO, l.log_to_ICAO, dep.apt_long_rad as depLONG, dep.apt_lat_rad as depLAT, arr.apt_long_rad as arrLONG, arr.apt_lat_rad as arrLAT 
					FROM ".DB_NAME.".log as l, ".DB_NAME.".Airports as dep, ".DB_NAME.".Airports as arr
					WHERE l.log_from_ICAO = dep.apt_ICAO AND l.log_to_ICAO = arr.apt_ICAO AND l.pilot_id = $session_pilot_id;";
					
					$totKM = 0;
                    $arr = execSQL($query);
					
                    foreach ($arr as $row) {
                    	//haversine method for calulating great-circle distance.
                    	//a = sin²(Δφ/2) + cos(φ1)⋅cos(φ2)⋅sin²(Δλ/2)
						//c = 2⋅atan2(√a, √(1−a))
						//d = R⋅c
						
						//where	φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
						//note that angles need to be in radians to pass to trig functions!
						
						
						$R = 6371; //KM
						$lat2 = $row['arrLAT'];
						$lat1 = $row['depLAT'];
						$lon2 = $row['arrLONG'];
						$lon1 = $row['depLONG'];
						$departure = $row['log_from_ICAO'];
						$arrival = $row['log_to_ICAO'];
						
						if (($arrival != $departure) and ($lat1 != 0) and ($lat2!=0)) {
														
							$totKM += haversine($lat1,$lat2,$lon1,$lon2);
						}
                    }
                    
                    $totMiles = Round($totKM/1.852,2);
					$timesArrowndTheWorld = Round($totKM / 40075,2); //40075km is the earth circumference
					
                    echo "<label>".$totMiles." NM which is equal to ".$timesArrowndTheWorld." laps arround the world</label>";
                ?>
                    
			</form>
			
			
			
		</div>
	</body>
</html>