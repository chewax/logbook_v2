<html>
	<?php
		include_once("../../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
	?>
	<head>
		<?php add_css_screen();?>
		<?php add_css_dataTables();?>
	</head>
	<body>
		<?php do_menu();?>
		<script>
			$(function(){
				$("#typesTable").dataTable();
				$('#typesTable').dataTable().columnFilter();
			})
		</script>
		<?php
            session_start();
            if (!empty($_SESSION['pilot_id'])){
                $session_pilot_id = $_SESSION['pilot_id'];
                $session_pilot_name = $_SESSION['pilot_name'];
            } else { 
                $session_pilot_id = 0;
                $session_pilot_name = "";
            }
        ?>
		<div class="display" id="reportsPage">   
			
			<form method="post" action="#">
				<label>Report:</label>
				<select class="dropdownlist" id="ReportOption">
					<option value="1">Grand Totals</option>
					<option value="2">Download CSV Hours Backup</option>
				</select>
				<input class="btn" type="button" name="submit" id="button" value="Process" onclick="switchReports()">
				<?php if(!empty($_SESSION['report_name'])){
					$name = $_SESSION['report_name'];
					echo '<a class="download" href="reports/'.$name.'">Download Last Report</a>';
				} ?>
			</form>
			
			
			
		</div>
	</body>
</html>