<html>
	<?php
		include_once("../../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
	?>
	<head>
		<?php add_css_screen();?>
		<?php add_css_dataTables();?>
	</head>
	<body>
		<?php do_menu();?>
		<script>
			$(function(){
				$("#typesTable").dataTable();
				$('#typesTable').dataTable().columnFilter();
			})
		</script>
		<?php
            session_start();
            if (!empty($_SESSION['pilot_id'])){
                $session_pilot_id = $_SESSION['pilot_id'];
                $session_pilot_name = $_SESSION['pilot_name'];
            } else { 
                $session_pilot_id = 0;
                $session_pilot_name = "";
            }
        ?>
		<div class="display" id="tabtypes">
            <div class="callout_form" id="typeForm">
                <form method="post" action="#">
                    <input id='textbox' type="text" placeholder="IATA, eg: B763" name="edIata"/>
                    <br>
                    <input id='textbox' type="text" placeholder="Dsc, eg: Boeing 767-300" name="edName"/>
                    <br>
                    Multi:
                    <select name="isMulti" id="dropdownlist">
                        <option id="selectbox" value="Y">YES</option>
                        <option id="selectbox" value="N">NO</option>
                    </select>
                    <br>
                    Complex:
                    <select name="isComplex" id="dropdownlist">
                        <option id="selectbox" value="Y">YES</option>
                        <option id="selectbox" value="N">NO</option>
                    </select>
                    <br>
                    Fixed Wing:
                    <select name="isFixedWing" id="dropdownlist">
                        <option id="selectbox" value="Y">YES</option>
                        <option id="selectbox" value="N">NO</option>
                    </select>
                    <br>
                    Lighter Than Air:
                    <select name="isLighterThanAir" id="dropdownlist">
                        <option id="selectbox" value="Y">YES</option>
                        <option id="selectbox" value="N">NO</option>
                    </select>
                    <br>
                    Land/Water:
                    <select name="isLand" id="dropdownlist">
                        <option id="selectbox" value="Y">LAND</option>
                        <option id="selectbox" value="N">WATER</option>
                        <option id="selectbox" value="B">BOTH</option>
                    </select>
                    <input type="button" name="submit" id="button" value="Insertar" onclick="insertType()">
                </form>	
            </div>
            
            <table class="grid" id="typesTable">
            <thead>
            <tr>
                <th class='grid'>Type id</th>
                <th class='grid'>Type IATA</th>
                <th class='grid'>Type Name</th>
                <th class='grid'>Type Is Multi</th>
                <th class='grid'>Type Is Fixed Wing</th>
                <th class='grid'>Type is Complex</th>
                <th class='grid'>Type is Land</th>
                
                <?php 
                    if ($session_pilot_id) {
                        echo '<th class="grid"><a href="javascript:void(0);" onclick="showTypeForm()" class="add">+</a></th>';
                    }else{
                        echo '<th class="grid"></th>';
                    }
                ?>
                
            </tr>
            </thead>
            <tbody>

                <?php
                
                $query = "SELECT * FROM ".DB_NAME.".aircraft_type WHERE pilot_id = $session_pilot_id;";

                $arr = execSQL($query);
                $i = 1;
                foreach ($arr as $row) {
                    ($i % 2)== 0 ? $class = "grid_evenrow" : $class = "grid_oddrow";

                    $type_id = $row['type_id'];
                    $type_iata = $row['type_iata'];
                    $type_name = $row['type_name'];
                    $type_is_multi = $row['type_is_multi'];
                    $type_is_complex = $row['type_is_complex'];
                    $type_fixed_wing = $row['type_fixed_wing'];
                    $type_land = $row['type_land'];

                    echo "<td class='$class'>$type_id</td>";
                    echo "<td class='$class'>$type_iata</td>";
                    echo "<td class='$class'>$type_name</td>";
                    echo "<td class='$class'>$type_is_multi</td>";
                    echo "<td class='$class'>$type_fixed_wing</td>";
                    echo "<td class='$class'>$type_is_complex</td>";
                    echo "<td class='$class'>$type_land</td>";
                    echo "<td class='$class'><a class='rowdel' onclick='deleteType($type_id)' href='javascript:void(0);'>delete</a></td>";
                    echo"</tr>";
                    $i++;
                }
            ?>
             
             </tbody>
             <tfoot>
             		<th id='footer'>Type id</th>
                	<th id='footer'>Type IATA</th>
                	<th id='footer'>Type Name</th>
                	<th id='footer'>Type Is Multi</th>
                	<th id='footer'>Type Is Fixed Wing</th>
                	<th id='footer'>Type is Complex</th>
                	<th id='footer'>Type is Land</th>
             </tfoot>
             </table>   

		</div>
	</body>
</html>