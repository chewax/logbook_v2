<html>
	<?php
		include_once("../../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
		
		//USER CONTROL
		session_start();
        if (!empty($_SESSION['pilot_id'])){
            $session_pilot_id = $_SESSION['pilot_id'];
            $session_pilot_name = $_SESSION['pilot_name'];
        } else { 
            $session_pilot_id = 0;
            $session_pilot_name = "";
        }
	?>
	<head>
		<!-- CSS -->
		<?php add_css_screen();?>
		<?php add_css_dataTables();?>
	</head>
	<body>
		<!-- MENU CODE-->
		<?php do_menu();?>
		<script>
			$(function($){
				$("#logblookTable").dataTable();
				$("#logblookTable").dataTable().columnFilter();
			});
			
			$(function($){
				$(".datetime").mask("9999-99-99 99:99");	
			});
		</script>
		
		<div class="display" id="tabLogbook">
            <div class="callout_form" id="logbookForm">
                <form method="post" action="#">
                	<?php
                		$query = "SELECT * FROM ".DB_NAME.".user_config where pilot_id = $session_pilot_id;";
						$arr = execSQL($query);
						foreach ($arr as $row) {
							$user_lang = $row['user_lang'];
							$user_duty_starts = $row['user_duty_starts'];
							$user_duty_ends = $row['user_duty_ends'];
							$user_def_apt = $row['user_def_apt'];
							$user_def_funct = $row['user_def_funct'];
							$user_def_flight_rules = $row['user_def_flight_rules'];
							$user_max_on_30 = $row['user_max_on_30'];
							$user_max_on_year = $row['user_max_on_year'];
							$user_def_fltno_prefix = $row['user_def_fltno_prefix'];
						}
					?>
                    <input id='textbox' type="text" placeholder="Flight #" name="edFltNo" value="<?php echo $user_def_fltno_prefix; ?>"/>
                    Aircraft:
                    <select name="logbookAircraftDDL" id="dropdownlist">
                    <?php
                        $query = "SELECT * FROM (".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t) 
                        WHERE t.type_id = a.type_id
                        AND $session_pilot_id = a.pilot_id;";
                        $arr = execSQL($query);
                        foreach ($arr as $row) {
                            $craft_id = $row['craft_id'];
                            $craft_reg = $row['craft_reg'];
                            $type_name = $row['type_name'];
                            echo "<option value='$craft_id'>$craft_reg - $type_name</option>";
                        }
                    ?>
                    </select>
                    <input id='textbox' type="text" placeholder="FROM, eg. KLAX" name="edFromIATA" value="<?php echo $user_def_apt; ?>"/>
                    <input id='textbox' type="text" placeholder="TO, eg. KSFO" name="edTOIATA"/>
					<input class="datetime" id='textbox' type="text" placeholder="OUT: YYYY-MM-DD HH:MM" name="edLogOUT"/>
                    <input class="datetime" id='textbox' type="text" placeholder="OFF: YYYY-MM-DD HH:MM" name="edLogOFF"/>
                    <input class="datetime" id='textbox' type="text" placeholder="ON: YYYY-MM-DD HH:MM" name="edLogON"/>
                    <input class="datetime" id='textbox' type="text" placeholder="IN: YYYY-MM-DD HH:MM" name="edLogIN"/>
                    
                    Function Onboard:
                    <select name="functionOnboardDDL" id="dropdownlist">
                        <?php 
                        echo $user_def_funct == "PIC" ? '<option value="PIC" selected="selected">PIC</option>': '<option value="PIC">PIC</option>';
                        echo $user_def_funct == "SIC" ? '<option value="SIC" selected="selected">SIC</option>': '<option value="PIC">SIC</option>';
                        echo $user_def_funct == "STU" ? '<option value="STU" selected="selected">STU</option>': '<option value="PIC">STU</option>';
                        echo $user_def_funct == "INS" ? '<option value="INS" selected="selected">INS</option>': '<option value="PIC">INS</option>';
                        ?>
                    </select>
                    Flight Duty:
                    <select name="flightDutyDDL" id="dropdownlist">
                        <option value="FLT" selected="selected">Flight</option>
                        <option value="DHD">Dead Head</option>
                        <option value="SIM">Simulator</option>
                    </select>
                    Flight Rules:
                    <select name="flightRulesDDL" id="dropdownlist">
                        <?php
                        echo $user_def_flight_rules = "VFR" ? '<option value="VFR" selected="selected">VFR</option>' : '<option value="VFR">VFR</option>';
						echo $user_def_flight_rules = "IFR" ? '<option value="IFR" selected="selected">IFR</option>' : '<option value="IFR">IFR</option>';
                        ?>
                    </select>
                    <input id='textbox' type="text" placeholder="Number of T/O" name="edNoTO"/>
                    <input id='textbox' type="text" placeholder="Number of Landings" name="edNoLAND"/>
                    Approach Type:
                    <select name="ApproachTypeDDL" id="dropdownlist">
                        <option value="" selected="selected">None</option>
                        <option value="ILSI">ILS CATI</option>
                        <option value="ILSII">ILS CATII</option>
                        <option value="ILSIII">ILS CATIII</option>
                        <option value="VOR">VOR</option>
                        <option value="VORD">VOR-DME</option>
                        <option value="NDB">NDB</option>
                    </select>
                    <textarea id="freetext" name="logbookCommentTA" placeholder="Comments"></textarea>
                    <input type="button" name="submit" id="button" value="Insert" onclick="insertLog()">
                </form>
            </div>
            <table class="grid" id="logblookTable">
            	<thead>
	            	<tr>
	                <!--<th class='grid'>Aircraft Id</th>
	                <th class='grid'>Pilot Id</th>-->
	                <th class='grid'>Flt.#</th>
	                <th class='grid'>Airplane</th>
	                <th class='grid'>Register</th>
	                <th class='grid'>FROM</th>
	                <th class='grid'>TO</th>
	                <th class='grid'>OUT</th>
	                <!--<th class='grid'>OFF</th>
	                <th class='grid'>ON</th>-->
	                <th class='grid'>IN</th>
	                <!--<th class='grid'>Function</th>
	                <th class='grid'>Duty</th>-->
	                <th class='grid'>Flight Rules</th>
	                <!--<th class='grid'>#T/O</th>
	                <th class='grid'>#Landings</th>
	                <th class='grid'>Approach</th>-->
	                <th class='grid'>Comments</th>
	                
	                <?php 
	                    if ($session_pilot_id) {
	                        echo '<th class="grid"><a href="javascript:void(0);" onclick="showLogbookForm()" class="add">+
	                        </a></th>';
	                    }else{
	                        echo '<th class="grid"></th>';
	                    }
	                ?>
	
	            	</tr>
				</thead>
				<tbody>
	                <?php
	                $query = "SELECT * FROM (".DB_NAME.".log as l, ".DB_NAME.".aircraft as a, ".DB_NAME.".aircraft_type as t) 
	                WHERE l.craft_id = a.craft_id 
	                AND a.type_id = t.type_id
	                AND l.pilot_id = $session_pilot_id
	                ORDER BY log_out;";
	
	                $arr = execSQL($query);
	                $i = 1;
	                foreach ($arr as $row) {
	                    ($i % 2)== 0 ? $class = "grid_evenrow" : $class = "grid_oddrow";
	                    
	                    echo "<tr>";
	                    $log_id = $row['log_id'];
	                    $craft_id = $row['craft_id'];
	                    $pilot_id = $row['pilot_id'];
	                    $log_flt_no = $row['log_flt_no'];
	                    $craft_reg = $row['craft_reg'];
	                    $type_name = $row['type_name'];
	                    $log_from_ICAO = $row['log_from_ICAO'];
	                    $log_to_ICAO = $row['log_to_ICAO'];
	                    $log_out = $row['log_out'];
	                    $log_off = $row['log_off'];
	                    $log_on = $row['log_on'];
	                    $log_in = $row['log_in'];
	                    $log_function = $row['log_function'];
	                    $log_flt_duty = $row['log_flt_duty'];
	                    $log_flight_rules = $row['log_flight_rules'];
	                    $log_TO = $row['log_TO'];
	                    $log_LAND = $row['log_LAND'];
	                    $log_app = $row['log_app'];
	                    $log_comm = $row['log_comm'];
	
	
	                    //echo "<td class='$class'>$craft_id</td>";
	                    //echo "<td class='$class'>$pilot_id</td>";
	                    echo "<td class='$class'>$log_flt_no</td>";
	                    echo "<td class='$class'>$type_name</td>";
	                    echo "<td class='$class'>$craft_reg</td>";
	                    
	                    echo "<td class='$class'>$log_from_ICAO</td>";
	                    echo "<td class='$class'>$log_to_ICAO</td>";
	                    echo "<td class='$class'>$log_out</td>";
	                    //echo "<td class='$class'>$log_off</td>";
	                    //echo "<td class='$class'>$log_on</td>";
	                    echo "<td class='$class'>$log_in</td>";
	                    //echo "<td class='$class'>$log_function</td>";
	                    //echo "<td class='$class'>$log_flt_duty</td>";
	                    echo "<td class='$class'>$log_flight_rules</td>";
	                    //echo "<td class='$class'>$log_TO</td>";
	                    //echo "<td class='$class'>$log_LAND</td>";
	                    //echo "<td class='$class'>$log_app</td>";
	                    echo "<td class='$class'>$log_comm</td>";
	                    
	                    echo "<td class='$class'><a class='rowdel' onclick='deleteLogEntry($log_id)' href='javascript:void(0);'>delete</a></td>";
	                    echo"</tr>";
	                    $i++;
	                }
	            ?>
	            </tbody>
	            <tfoot>
	            	 <!--<th id='footer'>Aircraft Id</th>
	                <th id='footer''>Pilot Id</th>-->
	                <th id='footer'>Flt.#</th>
	                <th id='footer'>Airplane</th>
	                <th id='footer'>Register</th>
	                <th id='footer'>FROM</th>
	                <th id='footer'>TO</th>
	                <th id='footer'>OUT</th>
	                <!--<th id='footer'>OFF</th>
	                <th id='footer''>ON</th>-->
	                <th id='footer'>IN</th>
	                <!--<th id='footer'>Function</th>
	                <th id='footer'>Duty</th>-->
	                <th id='footer'>Flight Rules</th>
	                <!--<th id='footer'>#T/O</th>
	                <th id='footer'>#Landings</th>
	                <th id='footer'>Approach</th>-->
	                <th id='footer'>Comments</th>
	            </tfoot>
             </table> 
        </div>
	</body>
</html>