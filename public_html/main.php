<html>
	<?php
		include_once("../resources/config.php");
		include_once("methods.inc");
		include_once("snippets.inc");
		include_once(PUBLIC_PATH."/modules/menu.php");
		include("user_agent.php");
		
		//USER CONTROL
        session_start();
        if (!empty($_SESSION['pilot_id'])){
            $session_pilot_id = $_SESSION['pilot_id'];
            $session_pilot_name = $_SESSION['pilot_name'];
			redirect('modules/flightTrends.php');
        } else { 
            $session_pilot_id = 0;
            $session_pilot_name = "";
        }
     ?>
	<head>
		<?php add_css_screen();?>
	</head>
	<body>
		<!-- Add Menu Code -->
		<?php do_menu();?>
	</body>
</html>